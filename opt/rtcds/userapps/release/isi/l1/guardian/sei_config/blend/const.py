from isiguardianlib import const as top_const
"""This is the main configuration file for the blend guardians.
The dictionary is organized by CHAMBER, CHAMBER_TYPE, name of state,
degrees of freedom, and then an FM.
The name of the state will be used by the Guardian node for that 
states name.
Please place a 0 in all DOFS that will not be changed by this node.

Make sure that you are consistent with your dofs, if not
a filter will be changed and then perhaps never changed back.
"""


BLEND_FILTER_SELECTION = {
    'ETMY' : {
        'BSC_ST1' : {
            '45MHZ' : {
                'X' : 2, ### THE BLEND # to turn on ( ie. BLEND1 - SUPERSENS1)
                'Y' : 2, 
                'Z' : 6, 
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
            'MH_105' : {
                'X' : 1, 
                'Y' : 1,
                'Z' : 6,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
        },
    },
    'ETMX' : {
        'BSC_ST1' : {
            '45MHZ' : {
                'X' : 2, 
                'Y' : 2,
                'Z' : 6,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
            'MH_105' : {
                'X' : 1,
                'Y' : 1,
                'Z' : 6,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
        },
    },
    'ITMY' : {
        'BSC_ST1' : {
            '45MHZ' : {
                'X' : 2, 
                'Y' : 2,
                'Z' : 6,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
            'MH_105' : {
                'X' : 1,
                'Y' : 1,
                'Z' : 6,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
        },
    },
    'ITMX' : {
        'BSC_ST1' : {
            '45MHZ' : {
                'X' : 2, 
                'Y' : 2,
                'Z' : 6,
                'RX' : 4,
                'RY' : 1,
                'RZ' : 4},
            'MH_105' : {
                'X' : 1,
                'Y' : 1, 
                'Z' : 6,
                'RX' : 4,
                'RY' : 1,
                'RZ' : 4},
        },
    },
    'BS' : {
        'BSC_ST1' : {
            '45MHZ' : {
                'X' : 2,
                'Y' : 2,
                'Z' : 6,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
            'MH_105' : {
                'X' : 1,
                'Y' : 1,
                'Z' : 6,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 4},
        },
    },
}

BLEND_CONFIGS = BLEND_FILTER_SELECTION[top_const.CHAMBER][top_const.CHAMBER_TYPE]
